import React from 'react';
import Card from 'react-bootstrap/Card';
import {Col, Image, Row} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";
import Moment from 'react-moment';

const Channel = ({channelId, thumbnail, title, description, country, publishedAt, selectChannel}) => {
    const history = useHistory();

    const search = async () => {
        const resp = await api.get('/channels', {
            params: {
                id: channelId,
                part: 'snippet,statistics,contentDetails',
            }
        });
        console.log('Received', resp.data.items);
        selectChannel(resp.data.items[0]);
        history.push(`/channels/${resp.data.items[0].id}`);
    };

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Row>
                <Col xs={3}>
                    <Image src={thumbnail.url}
                           fluid={true}
                           rounded
                           onClick={search}/>
                </Col>
                <Col>
                    <Card.Title onClick={search}>{title}</Card.Title>
                    <Card.Subtitle className={"mb-4 text-muted"}> Chaine créée le : 
                        <Moment date={publishedAt} format=" DD/MM/YYYY à hh:mm" />
                    </Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>)
};

export default Channel;