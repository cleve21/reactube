import React from "react";
import Card from "react-bootstrap/Card";
import { Col, Image, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { useHistory } from "react-router-dom";
import Moment from "react-moment";

const DetailedChannel = ({ id, snippet, statistics, onEscape }) => {
  const { title, description, publishedAt, thumbnails } = snippet;
  const { viewCount, videoCount, subscriberCount } = statistics;
  const history = useHistory();

  return (
    <Card style={{ width: "100%" }}>
      <Card.Body>
        <Button
          variant="danger"
          style={{ float: "right" }}
          onClick={history.goBack}
        >
          Retour
        </Button>
        <Row>
          <Col xs={3}>
            <Image src={thumbnails.high.url} fluid={true} rounded />
            <h6 className="mt-4 mb-2 text-muted">{viewCount} vues</h6>
            <h6 className="mt-4 mb-2 text-muted">{videoCount} vidéos</h6>
            <h6 className="mt-4 mb-2 text-muted">{subscriberCount} abonnés</h6>
          </Col>
          <Col>
            <h2>{title}</h2>

            <h5 className="mb-4 text-info">
              Chaine crée le{" "}
              <Moment date={publishedAt} format=" DD/MM/YYYY à hh:mm" />
            </h5>

            <Card.Text
              dangerouslySetInnerHTML={{
                __html: description.replace(/\n/g, "<br/>"),
              }}
            />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};

export default DetailedChannel;
