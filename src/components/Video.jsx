import React from 'react';
import Card from 'react-bootstrap/Card';
import {Col, Image, Row} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";
import Moment from 'react-moment';

const Video = ({videoId, thumbnail, title, description, channelTitle, publishTime, selectVideo}) => {
    const history = useHistory();

    const search = async () => {
        const resp = await api.get('/videos', {
            params: {
                id: videoId,
                part: 'snippet,statistics,contentDetails,player,recordingDetails,topicDetails',
            }
        });
        console.log('Received', resp.data.items);
        selectVideo(resp.data.items[0]);
        history.push(`/videos/${resp.data.items[0].id}`);
    };

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Row>
                <Col xs={3}>
                    <Image src={thumbnail.url}
                           fluid={true}
                           rounded
                           onClick={search}/>
                </Col>
                <Col>
                    <Card.Title onClick={search}>{title}</Card.Title>
                    <Card.Subtitle>{channelTitle}</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className={"mb-1 text-muted"}>
                        <Moment date={publishTime} format="DD/MM/YYYY hh:mm:ss" />
                    </Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>)
};

export default Video;